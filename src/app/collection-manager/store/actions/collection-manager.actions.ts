import { Action } from '@ngrx/store';

export enum CollectionManagerActionTypes {
  LoadCollections = '[Collection Page] Load Collections',
}

export class LoadCollections implements Action {
  readonly type = CollectionManagerActionTypes.LoadCollections;
}


export type CollectionManagerActions = LoadCollections;
