import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CollectionManagerEffects } from './collection-manager.effects';

describe('CollectionManagerEffects', () => {
  let actions$: Observable<any>;
  let effects: CollectionManagerEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CollectionManagerEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(CollectionManagerEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
