import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { concatMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { CollectionManagerActionTypes, CollectionManagerActions } from '../actions/collection-manager.actions';


@Injectable()
export class CollectionManagerEffects {


  @Effect()
  loadCollectionManagers$ = this.actions$.pipe(
    ofType(CollectionManagerActionTypes.LoadCollections),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    concatMap(() => EMPTY)
  );


  constructor(private actions$: Actions<CollectionManagerActions>) {}

}
