
import { CollectionManagerActions, CollectionManagerActionTypes } from '../actions/collection-manager.actions';

export interface State {

}

export const initialState: State = {

};

export function reducer(state = initialState, action: CollectionManagerActions): State {
  switch (action.type) {

    case CollectionManagerActionTypes.LoadCollections:
      return state;

    default:
      return state;
  }
}
