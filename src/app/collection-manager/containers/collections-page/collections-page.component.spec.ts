import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionsPageComponent } from './collections-page.component';
import { Store, StoreModule } from '@ngrx/store';

describe('CollectionsPageComponent', () => {
  let component: CollectionsPageComponent;
  let fixture: ComponentFixture<CollectionsPageComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ CollectionsPageComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionsPageComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
