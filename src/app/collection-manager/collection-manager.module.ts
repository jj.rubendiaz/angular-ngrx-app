import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCollectionManager from './store/reducers/collection-manager.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CollectionManagerEffects } from './store/effects/collection-manager.effects';
import { CollectionsPageComponent } from './containers/collections-page/collections-page.component';

@NgModule({
  declarations: [CollectionsPageComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('collectionManager', fromCollectionManager.reducer),
    EffectsModule.forFeature([CollectionManagerEffects])
  ]
})
export class CollectionManagerModule { }
