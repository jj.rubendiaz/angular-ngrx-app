import { Action } from '@ngrx/store';
 
export enum ActionTypes {
  Load = '[Product] Load',
  Edit = '[Product] Edit',
  Reset = '[Product] Reset',
  Success = '[Product API] Products Loaded Success',
  Error = '[Product API] Error',
  Activate = '[Product] Activate Product',
  GoToProduct = '[Product Router] Go To Product'
}
 
export class Load implements Action {
  readonly type = ActionTypes.Load;
}
 
export class Edit implements Action {
  readonly type = ActionTypes.Edit;
}
 
export class Reset implements Action {
  readonly type = ActionTypes.Reset;
}

export class Success implements Action {
  readonly type = ActionTypes.Success;

  constructor(public payload: any[]) {}
}

export class Error implements Action {
  readonly type = ActionTypes.Error;
}

export class Activate implements Action {
  readonly type = ActionTypes.Activate;

  constructor(public payload: string) {}
}

export class GoToProduct implements Action {
  readonly type = ActionTypes.GoToProduct;

  constructor(public payload: string) {}
}


export type ProductActions = Load | Edit | Reset | Success | Error | Activate;
