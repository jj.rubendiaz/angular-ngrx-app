import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, delay } from 'rxjs/operators';
import { ProductService } from '../providers/product.service';




@Injectable()
export class ProductEffects {

  @Effect()
  loadProducts$ = this.actions$
    .pipe(
      ofType('[Product] Load'),
      mergeMap(() => this.productService.getAll()
        .pipe(
          delay(500),
          map(products => ({
            type: '[Product API] Products Loaded Success', payload: products
          }),
          catchError(() => of({
            type: '[Product API] Products Loaded Error'
          }))
          )
        )
      
      )
    )



  constructor(private actions$: Actions, private productService: ProductService) {}

}
