import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { mergeMap, map, tap } from 'rxjs/operators';
import * as fromRouterStore from '@ngrx/router-store';
import * as fromProduct from '../actions/product.actions';
import { Router } from '@angular/router';



@Injectable()
export class RouterEffects {
  @Effect()
    goToItem$ = this.actions$
      .pipe(
        ofType(fromProduct.ActionTypes.GoToProduct),
        map((action: fromRouterStore.RouterRequestAction) => {
          this.router.navigate(['item']);
          return {type: fromProduct.ActionTypes.Activate, payload: action.payload}
        })
        )
  constructor(private actions$: Actions, private router: Router) { }

}
