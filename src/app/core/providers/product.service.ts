import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  ACCESS_KEY = '96a4f4030e045419f40be477fc02d14f5a50307021d9f5860e8411d789f0d39d';
  SECRET_KEY = '4e44dd88f1d5e25bc17bbcd1c456a8388b90ab24e1a1409cba42155afda74fda';

  constructor(private http: HttpClient) {
    
  }

  getAll() {
    const page = Math.round(Math.random() * 100);
    return this.http.get(`https://api.unsplash.com/search/photos/?client_id=${this.ACCESS_KEY}&query=clothing&page=${page}`)
      .pipe(
        map((photos: any) => {
          console.log('[PHOTOS] 📸 ==>', photos);
          return photos.results;
        })
      )
  }
}
