import * as fromProduct from '../reducers/product.reducer';
import { createSelector } from '@ngrx/store';

export const getProductState = createSelector(
    fromProduct.getProductsData
)