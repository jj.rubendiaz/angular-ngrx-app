import { Component, OnInit } from '@angular/core';
import { State, Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromProduct from '../reducers/product.reducer';

@Component({
  selector: 'app-collection-item',
  templateUrl: './collection-item.component.html',
  styleUrls: ['./collection-item.component.scss']
})
export class CollectionItemComponent implements OnInit {

  public activeProduct$: Observable<object>;

  constructor(private store: Store<{product: any[]}>) { }

  ngOnInit() {
    this.activeProduct$ = this.store.pipe(select('products'))
      .pipe(
        map(productState => (productState.data.entities[productState.active]))
      )
  }

}
