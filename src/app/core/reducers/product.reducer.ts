import * as fromProducts from '../actions/product.actions';

export interface Data {
  ids: string[],
  entities: object,
}


export interface ProductState {
  data: Data;
  loaded: boolean;
  loading: boolean;
  active?: string;
}

export const initialState: ProductState = {
  data: {ids: [], entities: {}},
  loaded: false,
  loading: false
};

export function productReducer(
  state = initialState, 
  action: fromProducts.ProductActions
  ): ProductState {
  switch (action.type) {
    case fromProducts.ActionTypes.Edit:
      return state;
 
    case fromProducts.ActionTypes.Reset:
      return initialState;
    
    case fromProducts.ActionTypes.Success:
    const idList = action.payload.map(item => item.id);
    const entitiesList = {};
    idList.forEach(id => {
      entitiesList[id] = action.payload.filter(item => item.id === id)[0];
    });
      return {
        ...state,
        data: {
          ...state.data,
          ids: idList,
          entities: entitiesList,
        },
        loading: false,
        loaded: true
      };
    case fromProducts.ActionTypes.Load:
      return {
        ...state,
        loading: true
      }
    case fromProducts.ActionTypes.Activate:
      let newEntities = {};
      state.data.ids.forEach(id => {
        newEntities[id] = {
          ...state.data.entities[id],
          active: false
        }
      });
      let newActive = {
        ...state.data.entities[action.payload],
        active: true
      };
      return {
        ...state,
        data: {
          ...state.data,
          entities: {
            ...newEntities,
            [action.payload]: newActive
          }
        },
        active: action.payload
      };
    default:
      return state;
  }
}

export const getProductsData = (state: ProductState) => state.data;
export const getActiveProduct = (state: ProductState) => state.active;
export const getProductsLoading = (state: ProductState) => state.loading;
export const getProductsLoaded = (state: ProductState) => state.loaded;


