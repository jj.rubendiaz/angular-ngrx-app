import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GoToProduct } from '../actions/product.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.scss']
})
export class CollectionListComponent {

  public collections$: Observable<any[]>;
  public collectionsIsLoading$: Observable<boolean>;

  constructor(private store: Store<{product: any[]}>, private router: Router) {
    this.collections$ = this.store.pipe(select('products'))
      .pipe(
        map(productState => productState.data.ids.map(id => {
          return productState.data.entities[id];
        }))
      )
    this.collectionsIsLoading$ = this.store.pipe(select('products'))
        .pipe(
          map(productState => productState.loading)
        )
  }

  activateProduct(id) {
    this.router.navigate(['item']);
    this.store.dispatch(new GoToProduct(id));
  }

}
