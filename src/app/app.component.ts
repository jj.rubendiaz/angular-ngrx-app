import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Load, Reset, Edit, Activate } from './core/actions/product.actions';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoadCollections } from './collection-manager/store/actions/collection-manager.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-checker';
  activeProduct$: Observable<any[]>;
  productLoading$: Observable<boolean>;

  constructor(private store: Store<{product: any[]}>, private router: Router) {
    this.activeProduct$ = this.store.pipe(select('products')).pipe(map(state => state.active || null));
    this.loadProducts();
  }

  loadProducts() {
    this.store.dispatch(new Load());
  }

  goToCollections() {
    this.router.navigate(['']);
  }

  editProducts() {
    this.store.dispatch(new Edit());
  }

  activeProduct() {
    this.router.navigate(['item']);
  }
}
