import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { AppComponent } from './app.component';
import { CollectionListComponent } from './core/collection-list/collection-list.component';
import { CollectionItemComponent } from './core/collection-item/collection-item.component';

const routes: Routes = [{
  path: 'item',
  component: CollectionItemComponent
}, {
  path: '**',
  component: CollectionListComponent
}];

@NgModule({
  imports: [
    StoreModule.forRoot({
      router: routerReducer
    }),
    RouterModule.forRoot(routes),
    // Connects RouterModule with StoreModule
    StoreRouterConnectingModule.forRoot(),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
