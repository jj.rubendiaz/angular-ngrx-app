import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
import { ProductEffects } from './core/effects/product.effects';
import { RouterEffects } from './core/effects/router.effects';
import { CollectionListComponent } from './core/collection-list/collection-list.component';
import { CollectionItemComponent } from './core/collection-item/collection-item.component';
import { CollectionManagerModule } from './collection-manager/collection-manager.module';

@NgModule({
  declarations: [
    AppComponent,
    CollectionListComponent,
    CollectionItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CollectionManagerModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([ProductEffects, RouterEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
