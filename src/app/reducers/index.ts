import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';

// Reducers
import * as fromProduct from '../core/reducers/product.reducer';
import * as fromRouter from '@ngrx/router-store';

export interface State {
  products: fromProduct.ProductState,
  router: fromRouter.RouterReducerState<any>
}

export const reducers: ActionReducerMap<State> = {
  products: fromProduct.productReducer,
  router: fromRouter.routerReducer
};

// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}
export const metaReducers: MetaReducer<State>[] = !environment.production ? [logger] : [];
